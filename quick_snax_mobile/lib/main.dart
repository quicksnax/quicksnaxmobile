import 'package:flutter/material.dart';

import 'ui/screens.dart';

void main() => runApp(new MaterialApp(
      title: 'Quick Snax Mobile',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashPage(),
      routes: <String, WidgetBuilder>{
        "/HomeScreen": (BuildContext context) => HomeScreen(),
        "/LoginScreen": (BuildContext context) => LoginScreen(),
        "/SnaxScreen": (BuildContext context) => SnaxScreen()
      },
    ));
