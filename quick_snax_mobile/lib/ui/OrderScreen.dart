import 'package:flutter/material.dart';
import 'package:quick_snax_mobile/controllers/controllers.dart';
import 'package:quick_snax_mobile/models/models.dart';
import 'package:quick_snax_mobile/utils/globals.dart' as globals;
import 'package:quick_snax_mobile/utils/constants.dart' as constants;

class OrderScreen extends StatefulWidget{
 OrderScreenState createState() => OrderScreenState();
}

class OrderScreenState extends State<OrderScreen>{
  
  TextEditingController _locationController = new TextEditingController();
  var _media;
  bool _progressBarState = false;
  static final _formKey = new GlobalKey<FormState>();
  
  @override
  Widget build(BuildContext context) {
   
    _media = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: new Center(child: new Text("Place order",style: TextStyle(color: Colors.white),),)
      ),
     body: Container(
          child: Column(
            children: <Widget>[
              new Container(
                width: _media != null
                    ? _media.size.width
                    : MediaQuery.of(context).size.width,
                margin:
                    const EdgeInsets.only(left: 40.0, right: 40.0, top: 10.0),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                        color: Colors.redAccent,
                        width: 0.5,
                        style: BorderStyle.solid),
                  ),
                ),
                padding: const EdgeInsets.only(left: 0.0, right: 10.0),
                child: new Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Expanded(
                      child:new Form(
                        key: _formKey,
                          child: TextFormField(
                        textAlign: TextAlign.left,
                        controller: _locationController,
                        keyboardType: TextInputType.text,
                        validator: (value){
                          if(value.isEmpty){
                            return "Please put in a location";
                          }
                        },
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          prefixIcon: Icon(Icons.location_city),
                          labelText: "Delivery location",
                          alignLabelWithHint: true,
                          hintText: 'Administration Building',
                          hintStyle: TextStyle(color: Colors.grey),
                        ),
                      ),
                    ),
                    ),
                  ],
                ),
              ),
              Divider(
                height: 24.0,
              ),
              Expanded(child: 
              renderOrderDetails(),),
              Container(
                    padding: EdgeInsets.only(top: 25.0, bottom: 25.0),
                    width: double.infinity,
                    margin: EdgeInsets.only(left: 20.0, right: 20.0),
                    child: RaisedButton(
                      clipBehavior: Clip.antiAlias,
                      color: Colors.greenAccent,
                      onPressed:() {
                        if(_formKey.currentState.validate()){
                            setState(() {
                      _progressBarState = true;
                    });
                    OrdersController.placeOrder(context,_locationController.text)
                        .then((result) {
                      setState(() {
                        if (_progressBarState) {
                          _progressBarState = false;
                        }
                      });
                      if(result){
                      Navigator.pop(context);
                      }
                    });
                  }
                },
                      elevation: 10.0,
                      shape:
                          RoundedRectangleBorder(borderRadius: BorderRadius.circular(35.0)),
                      child:  _progressBarState
                      ? const CircularProgressIndicator()
                      : ListTile(
                          title: new Center(
                        child: Text("Place order", style: TextStyle(fontSize: 25)),
                      ),trailing: Icon(Icons.arrow_forward),),
                    ),
                  ),
            ],
          ),
        ),
    );
  }

Widget renderOrderDetails(){
  return ListView.builder(
        shrinkWrap: true,  
        itemCount: globals.list.cartItems.length,
        itemBuilder: (context,i){
          return renderProductItem(globals.list.cartItems[i]);
        },
      );
}

Widget renderProductItem(CartItem product){
  return Container(
    padding: EdgeInsets.all(20.0),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        CircleAvatar(
      maxRadius: 40.0,
      backgroundImage: product.imageUrl != null? NetworkImage(product.imageUrl): AssetImage(constants.Constants.defaultProductImage)),
      new Container(
        alignment: Alignment.center,
        child:
            Column(
              children:[
                  new Text(product.name,style: TextStyle(
                  fontSize: 30.0,fontWeight: FontWeight.w600),),
                  Row(
                    children: <Widget>[ 
                      new Text("${(product.quantity)} for ",
                      style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.w300)),
                      new Text("\$ ${(product.price * product.quantity)}",
                      style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.w300)),

                    ]),
              ]))      
          ],
        ));
      }
}