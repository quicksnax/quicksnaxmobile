import 'package:flutter/material.dart';
import 'package:quick_snax_mobile/utils/constants.dart' as constants;
import 'package:quick_snax_mobile/utils/globals.dart' as globals;

class CommonDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(
              globals.loggedInUser.fullName,
            ),
            accountEmail: Text(
              globals.loggedInUser.userName,
            ),
            currentAccountPicture: new CircleAvatar(
              backgroundImage:
                  new AssetImage("resources/images/quicksnax-logo.png"),
            ),
          ),
          _listTile(),
        ],
      ),
    );
  }

  Widget _listTile() {
    if (globals.loggedInUser.role.compareTo(constants.Constants.ShopOwner) ==
        0) {
      return (new Column(
        children: <Widget>[
          new ListTile(
            title: Text(
              "Products",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.person,
              color: Colors.blue,
            ),
          ),
        ],
      ));
    } else if (globals.loggedInUser.role
            .compareTo(constants.Constants.Student) ==
        0) {
      return (new Column(
        children: <Widget>[
          new ListTile(
            title: Text(
              "Orders",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.fastfood,
              color: Colors.blue,
            ),
          ),
        ],
      ));
    } else if (globals.loggedInUser.role
            .compareTo(constants.Constants.DeliveryPerson) ==
        0) {
      return (new Column(
        children: <Widget>[
          new ListTile(
            title: Text(
              "Orders",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.shopping_cart,
              color: Colors.blue,
            ),
          ),
        ],
      ));
    }
    return (new ListView(
      children: <Widget>[
        new ListTile(
          title: Text(
            "Orders",
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
          ),
          leading: Icon(
            Icons.shopping_cart,
            color: Colors.blue,
          ),
        ),
      ],
    ));
  }
}
