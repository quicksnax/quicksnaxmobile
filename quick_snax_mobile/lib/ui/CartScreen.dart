import 'package:flutter/material.dart';
import 'package:quick_snax_mobile/models/models.dart';
import 'package:quick_snax_mobile/services/services.dart';
import 'package:quick_snax_mobile/ui/screens.dart';
import 'package:quick_snax_mobile/utils/constants.dart' as constants;
import 'package:quick_snax_mobile/utils/globals.dart' as globals;

class CartScreen extends StatefulWidget {
  @override
  CartScreenState createState() => CartScreenState();
}

class CartScreenState extends State<CartScreen> {
  var cartItems = globals.list.cartItems;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Items in cart"),
        backgroundColor: Colors.green,),
      body: cartItems.isNotEmpty
          ? Column(
              children:<Widget>[
                Expanded(child:renderList(),),
                 Container(
                    padding: EdgeInsets.only(top: 25.0, bottom: 25.0),
                    width: double.infinity,
                    margin: EdgeInsets.only(left: 20.0, right: 20.0),
                    child: RaisedButton(
                      clipBehavior: Clip.antiAlias,
                      color: Colors.greenAccent,
                      onPressed:(){
                        Navigator.of(context)
                        .pushReplacement(new MaterialPageRoute(builder: (context) => new OrderScreen()
                            ,settings: RouteSettings(name: "OrderScreen")));
                      },
                      elevation: 10.0,
                      shape:
                          RoundedRectangleBorder(borderRadius: BorderRadius.circular(35.0)),
                      child: ListTile(
                          title: new Center(
                        child: Text("Proceed to checkout", style: TextStyle(fontSize: 25)),
                      ),trailing: Icon(Icons.arrow_forward),),
                    ),
                  )
              ],
            )
          : new Container(
              color: Colors.white,
              child: new Center(
                child: new Text("Nothing added to cart"),
              )),
    );
  }

  Widget renderList() {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: globals.list.cartItems.length,
      itemBuilder: (context, i) {
        return _listItem(globals.list.cartItems[i]);
      },
    );
  }

  Widget _listItem(CartItem product) {
    if (product != null) {
      return Card(
          clipBehavior: Clip.antiAlias,
          elevation: 20.0,
          child: Row(
            children: <Widget>[
              Container(
                width: 250.0,
                height: 250.0,
                child: Image(
                    image: product.imageUrl != null
                        ? NetworkImage(product.imageUrl)
                        : constants.Constants.defaultProductImage),
              ),
              new Container(
                padding: EdgeInsets.only(left: 20.0),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Text(
                      "Product Name",
                      style: TextStyle(fontSize: 30.0),
                    ),
                    new Text(
                      "${product.name}",
                      style: TextStyle(fontSize: 19.0),
                    ),
                    Divider(
                      height: 24.0,
                    ),
                    new Text(
                      "Quantity",
                      style: TextStyle(fontSize: 30.0),
                    ),
                    new Text("${product.quantity}",
                        style: TextStyle(fontSize: 19.0)),
                    Divider(
                      height: 24.0,
                    ),
                    new Text(
                      "Total cost",
                      style: TextStyle(fontSize: 30.0),
                    ),
                    new Text(
                      "\$ ${(product.price * product.quantity)}",
                      style: TextStyle(fontSize: 19.0),
                    ),
                    Divider(
                      height: 24.0,
                    ),
                    new MaterialButton(
                      child: Text(
                        "Remove from cart",
                        style: TextStyle(fontSize: 20.0),
                      ),
                      onPressed: () {
                        globals.list.cartItems.remove(product);
                        setState(() {
                          cartItems = globals.list.cartItems;
                        });
                        Preferences.removeFromCart(globals.list);
                      },
                    )
                  ],
                ),
              )
            ],
          ));
    } else {
      return Center(
        child: new Text("Nothing added to cart"),
      );
    }
  }
}
