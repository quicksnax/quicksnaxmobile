import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:quick_snax_mobile/controllers/controllers.dart';
import 'package:quick_snax_mobile/models/models.dart';
import 'package:quick_snax_mobile/ui/custom_widgets/custom_widgets.dart';

class ProductScreen extends StatefulWidget {
  final Product product;

  const ProductScreen({Key key, this.product}) : super(key: key);

  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  var _media;
  bool _progressBarState = false;

  Product productToEdit;

  TextEditingController _productNameController;
  TextEditingController _productPriceController;

  TextEditingController _productQuantityController;

  List<String> _productTypes = ["Juice", "Sandwich", "Pastry", "Hot Beverage"];

  List<DropdownMenuItem<String>> _typesDropDownItems;
  String _currentType;
  File _productImage;
  static final _formKey = new GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _productNameController = new TextEditingController(
        text: widget.product != null ? widget.product.name : "");
    _productQuantityController = new TextEditingController(
        text: widget.product != null ? widget.product.inStock.toString() : "");
    _productPriceController = new TextEditingController(
        text: widget.product != null ? widget.product.price.toString() : "");
    _typesDropDownItems = getDropDownMenuItems();
    _currentType = widget.product == null
        ? _typesDropDownItems[0].value
        : _typesDropDownItems.contains(widget.product.type)
            ? widget.product.type
            : _typesDropDownItems[0].value;
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      maxHeight: 240.0,
      maxWidth: 240.0,
    );

    setState(() {
      _productImage = image;
    });
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String city in _productTypes) {
      items.add(new DropdownMenuItem(value: city, child: new Text(city)));
    }
    return items;
  }

  void changedDropDownItem(String selectedType) {
    setState(() {
      _currentType = selectedType;
    });
  }

  @override
  Widget build(BuildContext context) {
    _media = MediaQuery.of(context);
    return new Scaffold(
        appBar: new AppBar(
          title: Text(
            widget.product != null ? "Edit Product" : "Add Product",
          ),
          centerTitle: true,
          backgroundColor: Colors.green,
        ),
        body: new Container(
            margin: EdgeInsets.only(top: 20.0, left: 10.0, right: 10.0),
            child: SingleChildScrollView(
              padding: EdgeInsets.only(bottom: 30.0),
              scrollDirection: Axis.vertical,
              child: new Column(
                children: <Widget>[
                  Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 20.0, horizontal: 30.0),
                    width: double.infinity,
                    child: RaisedButton(
                      padding: EdgeInsets.all(12.0),
                      shape: StadiumBorder(),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new Text(
                            "Choose a photo",
                            style: TextStyle(fontSize: 20.0),
                          ),
                          new Icon(Icons.add_a_photo)
                        ],
                      ),
                      color: Colors.white,
                      onPressed: () {
                        getImage();
                      },
                    ),
                  ),
                  Container(
                    height: 200.0,
                    width: 200.0,
                    padding: EdgeInsets.only(bottom: 20.0),
                    child: Center(
                        child: _productImage == null
                            ? Text("No Image selected")
                            : Image.file(_productImage)),
                  ),
                  new Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 20.0, horizontal: 30.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "Product Category",
                          style: TextStyle(fontSize: 20.0),
                        ),
                        new DropdownButton(
                          items: _typesDropDownItems,
                          value: _currentType,
                          onChanged: changedDropDownItem,
                        )
                      ],
                    ),
                  ),
                  Form(
                      key: _formKey,
                      child: new Column(children: [
                    new Container(
                      width: _media != null
                          ? _media.size.width
                          : MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 10.0, right: 10.0, top: 10.0, bottom: 20.0),
                      alignment: Alignment.center,
                      decoration: ShapeDecoration.fromBoxDecoration(
                          BoxDecoration(
                              border: Border.all(
                                  color: Colors.green,
                                  width: 0.8,
                                  style: BorderStyle.solid),
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(30.0))),
                      /*decoration: ,*/
                      padding: const EdgeInsets.only(left: 0.0, right: 10.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              controller: _productNameController,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                prefixIcon: Icon(Icons.local_grocery_store),
                                labelText: "Product Name",
                                alignLabelWithHint: true,
                                hintText: 'Snickers',
                                hintStyle: TextStyle(color: Colors.grey),
                              ),
                              validator: (value){
                                if(value.isEmpty){
                                  return "Please put a product name";
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      width: _media != null
                          ? _media.size.width
                          : MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 10.0, right: 10.0, top: 10.0, bottom: 20.0),
                      alignment: Alignment.center,
                      decoration: ShapeDecoration.fromBoxDecoration(
                          BoxDecoration(
                              border: Border.all(
                                  color: Colors.green,
                                  width: 0.8,
                                  style: BorderStyle.solid),
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(30.0))),
                      padding: const EdgeInsets.only(left: 0.0, right: 10.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              controller: _productPriceController,
                              textAlign: TextAlign.left,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                prefixIcon: Icon(Icons.attach_money),
                                labelText: "Product Price",
                                alignLabelWithHint: true,
                                hintText: '\$100',
                                hintStyle: TextStyle(color: Colors.grey),
                              ),
                              validator: (value){
                                if(value.isEmpty){
                                  return "Please put a product name";
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    new Container(
                      width: _media != null
                          ? _media.size.width
                          : MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.only(
                          left: 10.0, right: 10.0, top: 10.0, bottom: 20.0),
                      alignment: Alignment.center,
                      decoration: ShapeDecoration.fromBoxDecoration(
                          BoxDecoration(
                              border: Border.all(
                                  color: Colors.green,
                                  width: 0.8,
                                  style: BorderStyle.solid),
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(30.0))),
                      padding: const EdgeInsets.only(left: 0.0, right: 10.0),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              controller: _productQuantityController,
                              textAlign: TextAlign.left,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                labelText: "Quantity",
                                alignLabelWithHint: true,
                                hintText: '100',
                                hintStyle: TextStyle(color: Colors.grey),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ])),
                  Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 20.0, horizontal: 30.0),
                    width: double.infinity,
                    child: RaisedButton(
                      padding: EdgeInsets.all(12.0),
                      shape: StadiumBorder(),
                      child: _progressBarState
                          ? const CircularProgressIndicator()
                          : Text(
                              "Save product",
                              style: TextStyle(
                                  color: Colors.white, fontSize: 20.0),
                            ),
                      color: Colors.green,
                      onPressed: () {
                        if(_productImage == null){
                          showAlertDialog(context, "Image not found", "Please select an Image", "Ok");
                        }
                        if (_formKey.currentState.validate() && _productImage != null) {
                          setState(() {
                            _progressBarState = true;
                          });
                          widget.product == null
                              ? ProductController.addNewProduct(
                              context,
                              _productNameController.text,
                              double.tryParse(
                                  _productPriceController.text),
                              _currentType,
                              int.tryParse(
                                  _productQuantityController.text),
                              _productImage)
                              .then((result) {
                            setState(() {
                              if (_progressBarState) {
                                _progressBarState = false;
                              }
                            });
                          })
                              : ProductController.editProduct(
                              widget.product.id,
                              context,
                              _productNameController.text,
                              double.tryParse(_productPriceController.text),
                              _currentType,
                              int.tryParse(_productQuantityController.text),
                              _productImage);
                        }
                      },
                    ),
                  ),
                ],
              ),
            )));
  }
}
