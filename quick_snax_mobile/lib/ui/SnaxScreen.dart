import 'package:flutter/material.dart';
import 'package:quick_snax_mobile/controllers/controllers.dart';
import 'package:quick_snax_mobile/models/models.dart';
import 'package:quick_snax_mobile/services/services.dart';
import 'package:quick_snax_mobile/ui/ProductScreen.dart';
import 'package:quick_snax_mobile/ui/widgets/custom_float.dart';
import 'package:quick_snax_mobile/utils/constants.dart' as constants;
import 'package:quick_snax_mobile/utils/globals.dart' as globals;
import 'package:quick_snax_mobile/viewmodels/SnaxViewModel.dart';
import 'package:scrollable_bottom_sheet/scrollable_bottom_sheet.dart';

class SnaxScreen extends StatefulWidget {
  @override
  SnaxScreenState createState() => new SnaxScreenState();
}

class SnaxScreenState extends State<SnaxScreen> with TickerProviderStateMixin {
  BuildContext _context;

  var role = globals.loggedInUser.role;


  String _currentState = "initial";
  String _currentDirection = "up";

  Product product;

  GlobalKey<AddToCartButtonState> _addToCartState = GlobalKey();

  Product getProduct() => product;

  int quantity = 0;
  callback(int newQuantity) {
    setState(() {
      quantity = newQuantity;
    });
    _addToCartState.currentState.updateQuantity(quantity);
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
        floatingActionButton: role.compareTo(constants.Constants.ShopOwner) == 0
            ? addProduct()
            : null,
        body: Container(
          child: bodyData(),
        ));
  }

  Widget addProduct() {
    return new FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(_context).push(
              new MaterialPageRoute(builder: (context) => new ProductScreen()));
        });
  }

  Widget imageStack(String img) => img != null
      ? new FadeInImage.assetNetwork(
          image: img,
          placeholder: constants.Constants.defaultProductImage,
        )
      : Image(
          image: AssetImage(constants.Constants.defaultProductImage),
        );

  //stack2
  Widget descStack(Product snax) => Positioned(
        bottom: 0.0,
        left: 0.0,
        right: 0.0,
        child: Container(
          decoration: BoxDecoration(color: Colors.black.withOpacity(0.5)),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Text(
                    snax.name,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                Text(snax.price.toString(),
                    style: TextStyle(
                        color: Colors.yellow,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold))
              ],
            ),
          ),
        ),
      );

  void editProduct(Product product) {
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (context) => new ProductScreen(
              product: product,
            )));
  }

_showDeleteSheet(Product product){
  showModalBottomSheet<void>(
    context: context,
    builder: (BuildContext context){
      return Container(
          child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(
                      Icons.delete,
                      color: Colors.redAccent,
                    ),
                    title: new Text("Delete snack"),
                    onTap: () {
                      ProductController.deleteProduct(context,product.id)
                          .then((result) {
                        if (result) {
                          Navigator.of(context).pop();
                          Scaffold.of(context).showSnackBar(SnackBar(content: new ListTile(title: new Text("Product has been deleted"), leading : new Icon(Icons.delete_sweep),)));
                        }
                      });
                    }),
                  ],
            ),
      );
    }
  );
}

  Widget productGrid(List<Product> snax) => GridView.count(
        crossAxisCount: 2,
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        children: snax
            .map((snax) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(20.0),
                    splashColor: Colors.yellow,
                    onLongPress:() => role.compareTo(constants.Constants.ShopOwner) ==0?{
                     _showDeleteSheet(snax)
                    }:null, 
                    onTap: () =>
                        role.compareTo(constants.Constants.Student) == 0
                            ? {
                                setState(() {
                                  product = snax;
                                }),
                                _showBottomSheet(snax)
                              }
                            : role.compareTo(constants.Constants.ShopOwner) ==0 ?  editProduct(snax):null,
                    child:  Material(
                      clipBehavior: Clip.antiAlias,
                      elevation: 2.0,
                      child: Stack(
                        fit: StackFit.expand,
                        children: <Widget>[
                          imageStack(snax.imageUrl),
                          descStack(snax),
                        ],
                      ),
                    ),
                  ),
                ))
            .toList(),
      );

  Widget _showProductDetails(BuildContext context, Product product) {
    return Stack(
      children: [
        ScrollableBottomSheet(
          halfHeight: 300.0,
          minimumHeight: 100.0,
          autoPop: true,
          scrollTo: ScrollState.half,
          snapAbove: true,
          snapBelow: true,
          callback: (state) {
            if (state == ScrollState.minimum) {
              _currentState = "minimum";
              _currentDirection = "up";
            } else if (state == ScrollState.half) {
              if (_currentState == "minimum") {
                _currentDirection = "up";
              } else {
                _currentDirection = "down";
              }
              _currentState = "half";
            } else {
              _currentState = "full";
              _currentDirection = "down";
            }
          },
          mayExceedChildHeight: true,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              new Container(
                height: 300.0,
                width: 300.0,
                child: new Center(
                  child: product.imageUrl != null
                      ? new FadeInImage.assetNetwork(
                          image: product.imageUrl,
                          placeholder: constants.Constants.defaultProductImage,
                        )
                      : Image(
                          image: AssetImage(
                              constants.Constants.defaultProductImage),
                          width: double.infinity,
                          height: 250.0,
                          fit: BoxFit.cover,
                        ),
                ),
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.only(left: 20.0),
                    child: new Text(
                      product.name,
                      style: TextStyle(fontSize: 40.0),
                    ),
                  ),
                  new Container(
                      padding: EdgeInsets.only(right: 40.0),
                      child: new Text(
                        "\$${product.price}",
                        style: TextStyle(fontSize: 30.0),
                      ))
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              new Text("Quantity", style: TextStyle(fontSize: 25)),
              SizedBox(
                height: 5.0,
              ),
              new QuantityWidget(
                callback: callback,
                product: product,
              ),
              SizedBox(
                height: 5.0,
              ),
              AddToCartButton(key:_addToCartState, product: product)
            ],
          ),
        )
      ],
    );
  }


  _showBottomSheet(Product product) {
    showBottomSheet<void>(
            context: context,
            builder: (context) => _showProductDetails(context, product))
        .closed
        .whenComplete(() {
      _QuantityWidgetState.setQuantity(0);
      _addToCartState.currentState.updateQuantity(0);
    });
  }

  Widget bodyData() {
    return FutureBuilder<SnaxViewModel>(
        future: ProductController.getProducts(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return snapshot.hasData
                ? productGrid(snapshot.data.snaxList)
                : Center(
                    child: Text(
                    "Currently no products found :(",
                  ));
          } else {
            return Center(
                child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.green),
            ));
          }
        });
  }
}

class AddToCartButton extends StatefulWidget{

  final Product product;

  const AddToCartButton({Key key, this.product}) : super(key: key);

  @override
  AddToCartButtonState createState() => AddToCartButtonState();
}

class AddToCartButtonState extends State<AddToCartButton>{

  bool _quantityGreaterThanZero = false;

  int _quantity = 0;

  updateQuantity(int quantity){
    setState((){
      _quantity = quantity;
      if(_quantity > 0 && _quantity <= widget.product.inStock){
        _quantityGreaterThanZero = true;
      }else{
        _quantityGreaterThanZero = false;
      }

    });
  }


  @override
  Widget build(BuildContext context) {
   return Container(
      padding: EdgeInsets.only(top: 25.0, bottom: 25.0),
      width: double.infinity,
      margin: EdgeInsets.only(left: 20.0, right: 20.0),
      child: RaisedButton(
        disabledColor: Colors.grey,
        clipBehavior: Clip.antiAlias,
        color: Colors.greenAccent,
        onPressed: _quantityGreaterThanZero
            ? () {
          globals.list.cartItems.add(new CartItem(
              widget.product.id,
              widget.product.name,
              widget.product.imageUrl,
              widget.product.price,
              _quantity));
          Preferences.addToCart(globals.list).whenComplete(() {
            Navigator.pop(context);
          });
        }
            : null,
        elevation: 10.0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(35.0)),
        child: ListTile(
            title: new Center(
              child: Text(_quantityGreaterThanZero?"Add to Cart":"Add quantity first", style: TextStyle(fontSize: 25)),
            )),
      ),
    );
  }

}

// ignore: must_be_immutable
class QuantityWidget extends StatefulWidget {
  Product product;
  Function(int) callback;
  QuantityWidget({this.callback, @required this.product});

  @override
  _QuantityWidgetState createState() => new _QuantityWidgetState();
}

class _QuantityWidgetState extends State<QuantityWidget> {
  var _context;
  static int _quantity = 0;

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Container(
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            _quantity > 0
                ? CustomFloat(
                    isMini: true,
                    icon: Icons.remove,
                    qrCallback: () {
                      setState(() {
                        _quantity--;
                      });
                      widget.callback(_quantity);
                    },
                  )
                : new Container(),
            new Text("$_quantity",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 25)),
            _quantity < widget.product.inStock
                ? CustomFloat(
                    isMini: true,
                    icon: Icons.add,
                    qrCallback: () {
                      setState(() {
                        _quantity++;
                      });
                      widget.callback(_quantity);
                    },
                  )
                : new Container()
          ],
        ));
  }
  static setQuantity(int quantity) {
    _quantity = quantity;
  }
  static int getQuantity() => _quantity;
}
