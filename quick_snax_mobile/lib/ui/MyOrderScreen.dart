import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:quick_snax_mobile/controllers/controllers.dart';
import 'package:quick_snax_mobile/models/models.dart';
import 'package:quick_snax_mobile/ui/OrderDetails.dart';
import 'package:quick_snax_mobile/utils/constants.dart' as constants;

class MyOrders extends StatefulWidget {
  @override
  _MyOrderState createState() => _MyOrderState();
}

class _MyOrderState extends State<MyOrders> {
  @override
  Widget build(BuildContext context) {
    return Container(child: getData());
  }

  List<Widget> renderList(List<PreviousOrdersModel> orders) {
    return orders
        .map((order) => new SliverStickyHeader(
              header: _buildHeader(order.orderDate),
              sliver: new SliverList(
                delegate: new SliverChildBuilderDelegate((context, x) {
                  return new ListTile(
                      onTap: () {
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (context) => new OrderDetailsScreen(
                                  id: order.previousOrders[x].orderId,
                                )));
                      },
                      title: new Text(
                        "Delivery location: ${order.previousOrders[x].location}",
                        style: TextStyle(
                            fontSize: 30.0, fontWeight: FontWeight.w500),
                      ),
                      subtitle: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            new Text(
                              "Order total: \$${order.previousOrders[x].orderTotal}",
                              style: TextStyle(
                                  fontSize: 25.0, fontWeight: FontWeight.w300),
                            ),
                            Divider(
                              height: 10.0,
                            ),
                            new Text(
                              "No. of items ${order.previousOrders[x].itemCount}",
                              style: TextStyle(
                                  fontSize: 25.0, fontWeight: FontWeight.w300),
                            )
                          ]),
                      trailing: new Container(
                          child: renderTrailingIcon(
                              order.previousOrders[x].orderStatus)));
                }, childCount: order.previousOrders.length),
              ),
            ))
        .toList();
  }

  Widget _buildHeader(String text) {
    return new Container(
      height: 40.0,
      color: Colors.lightGreen,
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      alignment: Alignment.centerLeft,
      child: new Text(
        "Order date: $text" ?? '',
        style: const TextStyle(color: Colors.white,fontSize:20.0, fontWeight: FontWeight.bold,fontStyle: FontStyle.italic),
      ),
    );
  }

  Widget renderTrailingIcon(String orderStatus) {
    switch (orderStatus) {
      case constants.Constants.Delivered:
        {
          return new Column(
            children: <Widget>[
          new Text(constants.Constants.Delivered),
          Icon(
            Icons.check,
            color: Colors.green,
            size: 30.0,
          )
          ],); 
          
        }
      case constants.Constants.InProgress:
        {
          return new Column(children: <Widget>[
            new Text(constants.Constants.InProgress),
            Icon(
            Icons.watch_later,
            color: Colors.orange,
            size: 30.0,
          )
          ],);
            
        }
      case constants.Constants.UnDelivered:
        {
          return new Column(children: <Widget>[
          new Text(constants.Constants.UnDelivered),
          Icon(
            Icons.close,
            color: Colors.red,
            size: 30.0,
          )
          ],);
            
        }
      default:
        {
          return new Column(children: <Widget>[
              new Text(constants.Constants.InProgress),
              Icon(
                  Icons.watch_later,
                  color: Colors.orange,
                  size: 30.0,
                )
          ],);
                
        }
    }
  }

  Widget getData() {
    return FutureBuilder<PreviousOrders>(
        future: OrdersController.getMyOrders(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              return CustomScrollView(
                slivers: renderList(snapshot.data.orders),
              );
            } else {
              return Center(child: Text("Order invalid"));
            }
          } else {
            return Center(
                child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.green),
            ));
          }
        });
  }
}
