import 'package:flutter/material.dart';
import 'package:quick_snax_mobile/models/UserModel.dart';
import 'package:quick_snax_mobile/models/models.dart';
import 'package:quick_snax_mobile/services/save_preferences.dart';
import 'package:quick_snax_mobile/ui/screens.dart';
import 'package:quick_snax_mobile/utils/constants.dart' as constants;
import 'package:quick_snax_mobile/utils/globals.dart' as globals;

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  int selectedIndex = 0;

  var _widgetOptions = [];

  List<Widget> getBottomNavBarItems() => _getBottomNavBarItems();

  @override
  void initState() {
    _widgetOptions = getBottomNavBarItems();
    super.initState();
  }

  List<Widget> _getBottomNavBarItems() {
    switch (globals.loggedInUser.role) {
      case constants.Constants.ShopOwner:
        return [SnaxScreen(), StudentsOrders()];
        break;
      case constants.Constants.DeliveryPerson:
        return [SnaxScreen(), StudentsOrders()];
        break;
      case constants.Constants.Student:
        return [SnaxScreen(), MyOrders()];
        break;
      default:
        return [SnaxScreen(), MyOrders()];
    }
  }

  Widget showCartIcon() {
    if (globals.loggedInUser.role.compareTo(constants.Constants.Student) == 0) {
      return IconButton(
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => new CartScreen(),settings: RouteSettings(name:"HomeScreen")));
        },
        icon: Icon(Icons.shopping_cart),
      );
    }
    return Container();
  }

  void handleSignOut() {
    Preferences.resetPreferences().then((canSignOut) {
    if(canSignOut){
      globals.loggedInUser = UserModel("", "", "", "", 0, "");
      globals.list = new CartList(new List<CartItem>());
      Navigator.of(context).pushReplacement(
        new MaterialPageRoute(builder: (context) => LoginScreen()));
    }else{
        HomeScreen();
      }
    });
  }

  void choiceAction(String choice) {
    if (choice.compareTo(constants.Constants.SignOut) == 0) {
      handleSignOut();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text("Quick Snax"),
        actions: <Widget>[
          SizedBox(
            width: 5.0,
          ),
          showCartIcon(),
          // overflow menu
          PopupMenuButton<String>(
            onSelected: choiceAction,
            itemBuilder: (BuildContext context) {
              return constants.Constants.choices.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: Container(
        color: Colors.white,
        height: double.infinity,
        padding: EdgeInsets.only(top: 10),
        child: _widgetOptions.elementAt(selectedIndex),
      ),
      bottomNavigationBar: bottomNavBar(),
    );
  }

  Widget bottomNavBar() {
    var role = globals.loggedInUser.role;
    if (role.compareTo(constants.Constants.ShopOwner) == 0) {
      return BottomNavigationBar(
        fixedColor: Colors.green,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.fastfood), title: Text('Products')),
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_basket), title: Text('Student Orders'))
        ],
        currentIndex: selectedIndex,
        onTap: _onItemTapped,
      );
    } else if (role.compareTo(constants.Constants.Student) == 0) {
      return BottomNavigationBar(
        fixedColor: Colors.green,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.fastfood), title: Text('All Products')),
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_basket), title: Text('Your Orders')),
        ],
        currentIndex: selectedIndex,
        onTap: _onItemTapped,
      );
    } else if (role.compareTo(constants.Constants.DeliveryPerson) == 0) {
      return BottomNavigationBar(
        fixedColor: Colors.green,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.fastfood), title: Text('All Products')),
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_basket), title: Text('Student Orders'))
        ],
        currentIndex: selectedIndex,
        onTap: _onItemTapped,
      );
    }
    return BottomNavigationBar(
      fixedColor: Colors.green,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(Icons.fastfood), title: Text('All Products')),
        BottomNavigationBarItem(
            icon: Icon(Icons.shopping_basket), title: Text('Student Orders'))
      ],
      currentIndex: selectedIndex,
      onTap: _onItemTapped,
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }
}
