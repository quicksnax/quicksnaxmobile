import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:quick_snax_mobile/controllers/OrderController.dart';
import 'package:quick_snax_mobile/models/models.dart';
import 'package:quick_snax_mobile/ui/screens.dart';
import 'package:quick_snax_mobile/utils/constants.dart' as constants;

class StudentsOrders extends StatefulWidget {
  @override
  _StudentsOrdersState createState() => _StudentsOrdersState();
}

class _StudentsOrdersState extends State<StudentsOrders>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(builder: (BuildContext context) {
        return Container(
          child: getData(),
        );
      }),
    );
  }

  Widget renderTrailingIcon(String orderStatus) {
    switch (orderStatus) {
      case constants.Constants.Delivered:
        {
          return new Column(
            children: <Widget>[
          new Text(constants.Constants.Delivered),
          Icon(
            Icons.check,
            color: Colors.green,
            size: 30.0,
          )
          ],); 
          
        }
      case constants.Constants.InProgress:
        {
          return new Column(children: <Widget>[
            new Text(constants.Constants.InProgress),
            Icon(
            Icons.watch_later,
            color: Colors.orange,
            size: 30.0,
          )
          ],);
            
        }
      case constants.Constants.UnDelivered:
        {
          return new Column(children: <Widget>[
          new Text(constants.Constants.UnDelivered),
          Icon(
            Icons.close,
            color: Colors.red,
            size: 30.0,
          )
          ],);
            
        }
      default:
        {
          return new Column(children: <Widget>[
              new Text(constants.Constants.InProgress),
              Icon(
                  Icons.watch_later,
                  color: Colors.orange,
                  size: 30.0,
                )
          ],);
                
        }
    }
  }

  List<Widget> renderList(List<Orders> orders) {
    return orders
        .map((order) => new SliverStickyHeader(
            header: _buildHeader(order.orderDate),
            sliver: new SliverList(
              delegate: new SliverChildBuilderDelegate((context, x) {
                return new ListTile(
                    onTap: () {
                      Navigator.of(context).push(new MaterialPageRoute(
                          builder: (context) => new OrderDetailsScreen(
                            id: order.orders[x].id,
                          )));
                    },
                    title: new Text("Delivery location: ${order.orders[x].location}",
                        style: TextStyle(
                            fontSize: 30.0, fontWeight: FontWeight.w500)),
                    subtitle: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                      new Text("Order total: \$${order.orders[x].total.roundToDouble()}",
                          style: TextStyle(
                              fontSize: 25.0, fontWeight: FontWeight.w300)),
                      Divider(
                        height: 10.0,
                      ),
                      new Text("No. of items ${order.orders[x].itemCount}",
                          style: TextStyle(
                              fontSize: 25.0, fontWeight: FontWeight.w300))
                    ]),
                    trailing:
                        renderTrailingIcon(order.orders[x].statusOfOrder));
              }, childCount: order.orders.length),
            )))
        .toList();
  }

  Widget _buildHeader(String text) {
    return new Container(
      height: 40.0,
      color: Colors.lightGreen,
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      alignment: Alignment.centerLeft,
      child: new Text(
        "Order date: $text" ?? '',
        style: const TextStyle(color: Colors.white,fontSize:20.0, fontWeight: FontWeight.bold,fontStyle: FontStyle.italic),
      ),
    );
  }

  Widget renderListItem(Order order) => ListTile(
      title: new Text(order.studentName),
      subtitle: new Text("To be delivered at ${order.location ?? ""}"),
      onTap: () {
        Navigator.of(context).push(new MaterialPageRoute(
            builder: (context) => new OrderDetailsScreen(
                  id: order.id,
                )));
      },
      trailing: new IconButton(
          icon: Icon(Icons.arrow_right),
          onPressed: () {
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (context) => new OrderDetailsScreen(id: order.id)));
          }));

  Widget getData() {
    return FutureBuilder<OrdersByDate>(
        future: OrdersController.getAllOrders(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              return CustomScrollView(
                slivers: renderList(snapshot.data.orders),
              );
            } else {
              return Center(child: Text("Order number invalid"));
            }
          } else {
            return Center(
                child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.green),
            ));
          }
        });
  }
}
