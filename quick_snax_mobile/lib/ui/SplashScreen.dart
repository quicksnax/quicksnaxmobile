import 'package:flutter/material.dart';
import 'package:quick_snax_mobile/models/models.dart';
import 'package:quick_snax_mobile/services/services.dart';
import 'package:quick_snax_mobile/utils/constants.dart' as constants;
import 'package:quick_snax_mobile/utils/globals.dart' as globals;
import 'package:splashscreen/splashscreen.dart';

import 'screens.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  var loggedIn = false;
  var userName = "";

  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 5,
        navigateAfterSeconds: loggedIn ? HomeScreen() : LoginScreen(),
        title: new Text(
          "Welcome to QuickSnax Mobile",
          style: new TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
        image: new Image(image: AssetImage(constants.Constants.defaultImage)),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        loaderColor: Colors.green);
  }

  @override
  void initState() {
    super.initState();
    Preferences.getPreferences().then((result) {
      var token = result.token;
      if (token.isEmpty) {
        setState(() {
          loggedIn = false;
          userName = "";
        });
      } else {
        var expiryTime = parseJwt(token)["exp"];
        if (validateToken(expiryTime)) {
          loggedIn = false;
          userName = "";
        } else {
          Preferences.getCartItems().timeout(Duration(seconds:10));
          setState(() {
            loggedIn = true;
            userName = result.userName;
          });
          globals.loggedInUser = UserModel.createUser(token);
        }
      }
    });
  }
}
