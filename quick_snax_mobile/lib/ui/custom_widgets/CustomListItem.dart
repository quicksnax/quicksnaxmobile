abstract class ListItem {}

class HeadingItem implements ListItem {
  final String heading;

  HeadingItem(this.heading);
}

class MessageItem implements ListItem {
  final String title;
  final String subtitle;

  MessageItem(this.title, this.subtitle);
}
