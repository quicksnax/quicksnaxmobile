import 'package:flutter/material.dart';
import 'package:quick_snax_mobile/controllers/OrderController.dart';
import 'package:quick_snax_mobile/models/models.dart';
import 'package:quick_snax_mobile/utils/constants.dart' as constants;
import 'package:quick_snax_mobile/utils/globals.dart' as globals;

class OrderDetailsScreen extends StatefulWidget {
  final int id;

  const OrderDetailsScreen({Key key, @required this.id}) : super(key: key);

  @override
  _OrderDetailsScreenState createState() => _OrderDetailsScreenState();
}

class _OrderDetailsScreenState extends State<OrderDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('Order Details'),
        backgroundColor: Colors.green,
      ),
      body: SingleChildScrollView(
        child: getOrderDetails(widget.id),
      ),
    );
  }

  Widget renderOrderDetails(SingleStudentOrderDetails details) {
    return Container(
        padding: EdgeInsets.all(20.0),
        child: new Column(
          children: <Widget>[
            new Text(
              "ORDER DETAILS",
              style: TextStyle(fontSize: 35.0, fontWeight: FontWeight.bold),
            ),
            new Divider(
              height: 100.0,
            ),
            new Container(
              margin: EdgeInsets.only(top: 30.0),
              padding: EdgeInsets.only(left: 10.0, right: 10.0),
              child: Column(
                children: <Widget>[
                  new Container(
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text("Student name",
                            style: TextStyle(
                                fontSize: 22.5, fontWeight: FontWeight.bold)),
                        new Text(
                          details.studentName ?? "",
                          style: TextStyle(
                              fontSize: 25.0, fontWeight: FontWeight.w300),
                        ),
                      ],
                    ),
                  ),
                  Divider(
                    height: 24.0,
                  ),
                  new Container(
                    padding: EdgeInsets.only(right: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text("Student ID Number",
                            style: TextStyle(
                                fontSize: 22.5, fontWeight: FontWeight.bold)),
                        new Text(
                          details.idNumber ?? "",
                          style: TextStyle(
                              fontSize: 25.0, fontWeight: FontWeight.w300),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Divider(
              height: 50.0,
            ),
            new Container(
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  new Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "Order quantity",
                          style: TextStyle(
                              fontSize: 22.5, fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          details.products.length.toString(),
                          style: TextStyle(
                              fontSize: 22.5, fontWeight: FontWeight.w300),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            renderProducts(details.products),
            Divider(
              height: 50.0,
            ),
            renderStatusButton()
          ],
        ));
  }

  Widget renderStatusButton() {
    return globals.loggedInUser.role.compareTo(constants.Constants.Student) != 0
        ? Container(
            width: double.infinity,
            child: RaisedButton(
                clipBehavior: Clip.antiAlias,
                color: Colors.greenAccent,
                onPressed: () {
                  _showBottomSheet();
                },
                elevation: 10.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(35.0)),
                child: ListTile(
                  title: new Center(
                    child: Text("Change order status",
                        style: TextStyle(fontSize: 25)),
                  ),
                )))
        : Container();
  }

  void _showBottomSheet() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(
                      Icons.check,
                      color: Colors.green,
                    ),
                    title: new Text("Change to delivered"),
                    onTap: () {
                      OrdersController.changeOrderStatus(context,
                              widget.id, constants.Constants.Delivered)
                          .then((result) {
                        if (result) {
                          Navigator.of(context).pop();
                          Scaffold.of(context).showSnackBar(
                          SnackBar(content: 
                          new ListTile(
                            title:new Text("Order Status updated to ${constants.Constants.Delivered}"),
                            trailing: new Icon(Icons.check,color: Colors.green,),)));
                        }
                      });
                    }),
                new ListTile(
                  leading: new Icon(
                    Icons.watch_later,
                    color: Colors.orange,
                  ),
                  title: new Text("Change to processing"),
                  onTap: () {
                    OrdersController.changeOrderStatus(context,
                            widget.id, constants.Constants.InProgress)
                        .then((result) {
                      if (result) {
                        Navigator.of(context).pop();
                        Scaffold.of(context).showSnackBar(
                          SnackBar(content: 
                          new ListTile(
                            title:new Text("Order Status updated to ${constants.Constants.InProgress}"),
                            trailing: new Icon(Icons.watch_later,color: Colors.orangeAccent,),)));
                      }
                    });
                  },
                ),
              ],
            ),
          );
        });
  }

  Widget renderProducts(List<SingleOrderProduct> products) {
    return Container(
      child: ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemCount: products.length,
        itemBuilder: (context, i) {
          return renderProductItem(products[i]);
        },
      ),
    );
  }

  Widget renderProductItem(SingleOrderProduct product) {
    return Container(
        padding: EdgeInsets.all(20.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            CircleAvatar(
                maxRadius: 40.0,
                backgroundImage: product.productImage != null
                    ? NetworkImage(product.productImage)
                    : AssetImage(constants.Constants.defaultProductImage)),
            new Container(
                alignment: Alignment.center,
                child: Column(children: [
                  new Text(
                    product.productName,
                    style:
                        TextStyle(fontSize: 30.0, fontWeight: FontWeight.w600),
                  ),
                  new Text(
                    "Quantity: ${product.quantity}",
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.w300),
                  ),
                  new Text("\$ ${product.price * product.quantity}",
                      style: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.w300)),
                ]))
          ],
        ));
  }

  Widget getOrderDetails(int id) {
    return FutureBuilder<SingleStudentOrderDetails>(
      future: OrdersController.getStudentOrder(id),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return snapshot.hasData
              ? renderOrderDetails(snapshot.data)
              : Center(child: Text("No Unfulfilled Student Orders"));
        } else {
          return Center(
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.green),
            ),
          );
        }
      },
    );
  }
}
