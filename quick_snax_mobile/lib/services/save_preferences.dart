import 'dart:convert';
import 'package:quick_snax_mobile/models/models.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:quick_snax_mobile/utils/globals.dart' as globals;


class Preferences {
  static const tokenPref = "token";
  static const usernamePref = "userName";
  static const cartItemsPref = "cartItems";

  static saveCurrentLogin(Map responseJson) async {
    //gets an instance of shared preferences to save user data to phone
    SharedPreferences preferences = await SharedPreferences.getInstance();

    var token = (responseJson != null && responseJson.isNotEmpty)
        ? LoginModel.fromJson(responseJson).token
        : "";
    var username = (responseJson != null && responseJson.isNotEmpty)
        ? LoginModel.fromJson(responseJson).userName
        : "";

    await preferences.setString(
        tokenPref, (token != null && token.length > 0) ? token : "");

    await preferences.setString(usernamePref,
        (username != null && username.length > 0) ? username : "");
  }

  static Future<UserModel> getPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    try{
    var token = (preferences.getString(tokenPref) ?? "");
    var user = UserModel.createUser(token);

    return user;
    }catch(e){
      print(e);
    }
    return new UserModel("", "", "", "", 0, "");
  }

static Future getCartItems() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    try {
      var cart = preferences.getString(cartItemsPref)??"";
      print(cart);
      if(cart.isNotEmpty){
        Map<String,dynamic> itemResponse = json.decode(cart);
        print(itemResponse);
        var items = new CartList.fromJson(itemResponse);
        print(items);
        globals.list.cartItems.addAll(items.cartItems);
      }
       
    } catch (e) {
      print(e);
    }

}

  static Future<bool>removeFromCart(CartList cartItems) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    try{
      var cart = jsonEncode(cartItems);
      print(cart);
     await preferences.setString(cartItemsPref, cart);
      return true;
    }catch(e){
      return false;
    }
  }


  static Future<bool> addToCart(CartList cartItems) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    try{
      var cartMap = jsonEncode(cartItems);
      print(cartMap);
    await preferences.setString(cartItemsPref, cartMap);
      return true;
    }catch(e){
      print(e);
      return false;
    }
  }

  static Future<bool> resetPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    try {
      preferences.remove(tokenPref);
      preferences.remove(usernamePref);
      preferences.remove(cartItemsPref);
    } catch (e) {
      return false;
    }

    return true;
  }
}
