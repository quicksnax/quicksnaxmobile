import 'dart:convert';
import 'dart:core';

Map<String, dynamic> parseJwt(String token) {
  final parts = token.split('.');
  if (parts.length != 3) {
    throw Exception("Invalid token");
  }
  final payload = _decodeBase64(parts[1]);
  final payloadMap = json.decode(payload);
  if (payloadMap is! Map<String, dynamic>) {
    throw Exception('invalid payload');
  }
  return payloadMap;
}

String _decodeBase64(String payload) {
  String output = payload.replaceAll('-', '+').replaceAll('_', '/');
  switch (output.length % 4) {
    case 0:
      break;
    case 2:
      output += '==';
      break;
    case 3:
      output += '=';
      break;
    default:
      throw Exception('Illegal base64url string');
  }
  return utf8.decode(base64Url.decode(output));
}

bool validateToken(int expiryTime) {
  expiryTime = expiryTime * 1000;
  DateTime expiryDate =
      new DateTime.fromMillisecondsSinceEpoch(expiryTime, isUtc: true);
  var currentDate = new DateTime.now();
  if (expiryDate.isBefore(currentDate)) {
    return true;
  }
  return false;
}
