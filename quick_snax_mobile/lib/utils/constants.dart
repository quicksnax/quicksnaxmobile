class Constants {
  //api url
  static const String ApiUrl = "http://0.tcp.ngrok.io:11229/api";

  //Roles
  static const String ShopOwner = "ShopOwner";
  static const String DeliveryPerson = "DeliveryPerson";
  static const String Student = "Student";

  static const resourcesDirectory = "resources/images";
  static const defaultImage = "$resourcesDirectory/quicksnax-logo.png";
  static const defaultProductImage = "$resourcesDirectory/default-product.png";

  static const SignOut = "Log out";

  static const UnDelivered = "Undelivered";

  static const  InProgress = "In progress";

  static const  Delivered = "Delivered";

  static const List<String> choices = <String>[SignOut];
}
