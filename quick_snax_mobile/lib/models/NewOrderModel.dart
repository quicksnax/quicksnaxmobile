class NewOrderModel {
  final String location;
  List<NewOrderProducts> products;

  NewOrderModel(this.location);

  Map<String, dynamic> toJson() => {"location": location,"productsOrder": products};
}
class NewOrderProducts{
  final int id;
  final int quantity;

  NewOrderProducts(this.id, this.quantity);

  Map<String, dynamic> toJson() => {"productId": id,"quantity": quantity};
}