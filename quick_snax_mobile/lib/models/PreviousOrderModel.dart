class PreviousOrders {
  List<PreviousOrdersModel> orders;

  PreviousOrders({this.orders});
}

class PreviousOrdersModel {
  final String orderDate;
  List<PreviousOrderModel> previousOrders;

  PreviousOrdersModel(this.orderDate);

  PreviousOrdersModel.fromJson(Map<String, dynamic> json)
      : orderDate = json['orderDate'],
        previousOrders = json['previousOrders']
            .map<PreviousOrderModel>(
                (order) => PreviousOrderModel.fromJson(order))
            .toList();
}

class PreviousOrderModel {
  final int orderId;
  final String location;
  final String timeOfOrder;
  final int itemCount;
  final double orderTotal;
  final String orderStatus;

  PreviousOrderModel(this.orderId, this.location, this.timeOfOrder,
      this.orderTotal, this.orderStatus, this.itemCount);

  PreviousOrderModel.fromJson(Map<String, dynamic> json)
      : orderId = json['orderId'],
        location = json['location'],
        timeOfOrder = json['timeOfOrder'],
        orderTotal = json['orderTotal'],
        itemCount = json['productAmount'],
        orderStatus = json['orderStatus'];
}
