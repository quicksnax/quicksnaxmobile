class LoginModel {
  final String userName;
  final String token;

  LoginModel(this.userName, this.token);

  LoginModel.fromJson(Map<String, dynamic> json)
      : userName = json['userName'],
        token = json['token'];

  Map<String, dynamic> toJson() => {'userName': userName, 'token': token};
}
