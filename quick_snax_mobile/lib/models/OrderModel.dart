class OrdersByDate{
  List<Orders> orders;
  OrdersByDate({this.orders}); 
}
class Order {
  final int id;
  final String location;
  final String timeOfOrder;
  final String statusOfOrder;
  final int itemCount;
  final double total;
  final String studentName;

  Order(this.id, this.location, this.studentName,this.statusOfOrder, this.timeOfOrder,this.itemCount,this.total);

  Order.fromJson(Map<String, dynamic> json)
      : id = json["orderId"],
        location = json["location"],
        timeOfOrder = json['timeOfOrder'],
        statusOfOrder = json['orderStatus'],
        itemCount = json['itemCount'],
        total = json['total'],
        studentName = json["studentName"];
}

class Orders {
  final String orderDate;
  List<Order> orders;

  Orders(this.orderDate);

  Orders.fromJson(Map<String, dynamic> json) 
    : orderDate = json['orderDate'],
    orders = json['orders']
                    .map<Order>((order)=>
                     Order.fromJson(order)).toList();
}
