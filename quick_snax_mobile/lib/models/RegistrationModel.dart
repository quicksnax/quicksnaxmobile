class RegistrationModel {
  final String idNumber;
  final String firstName;
  final String lastName;
  final String email;
  final String password;
  final String confirmPassword;

  RegistrationModel(this.idNumber, this.firstName, this.lastName, this.email,
      this.password, this.confirmPassword);

  Map<String, dynamic> toJson() => {
        "IdNumber": idNumber,
        "FirstName": firstName,
        "LastName": lastName,
        "Email": email,
        "Password": password,
      };
}
