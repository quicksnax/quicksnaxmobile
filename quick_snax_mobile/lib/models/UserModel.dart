import 'package:quick_snax_mobile/services/services.dart';

class UserModel {
  final String userName;
  final String userId;
  final String fullName;
  final String role;
  final int expirationTime;
  final String token;

  UserModel(this.userName, this.userId, this.fullName, this.role,
      this.expirationTime, this.token);

  static UserModel createUser(String token) {
    if (token.isNotEmpty) {
      var parsedToken = parseJwt(token);
      var expirationTime =
          parsedToken.containsKey("exp") ? parsedToken["exp"] : "";
      var userId = parsedToken.containsKey("sub") ? parsedToken["sub"] : "";
      var userName =
          parsedToken.containsKey("email") ? parsedToken["email"] : "";
      var role = parsedToken.containsKey("role") ? parsedToken["role"] : "";
      var fullName = parsedToken.containsKey("given_name") &&
              parsedToken.containsKey("LastName")
          ? parsedToken["given_name"] + " " + parsedToken["LastName"]
          : "";
      return UserModel(userName, userId, fullName, role, expirationTime, token);
    }
    return UserModel("", "", "", "", 0, "");
  }
}
