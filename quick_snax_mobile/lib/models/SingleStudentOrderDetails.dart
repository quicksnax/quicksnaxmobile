class SingleStudentOrderDetails {
  final int orderId;
  final String studentName;
  final String location;
  final String idNumber;
  final double total;

  List<SingleOrderProduct> products;

  SingleStudentOrderDetails(
      this.orderId, this.studentName, this.location, this.idNumber, this.total);

  SingleStudentOrderDetails.fromJson(Map<String, dynamic> json)
      : orderId = json['orderId'],
        studentName = json['studentName'],
        location = json['location'],
        idNumber = json['idNumber'],
        total = json['total'],
        products = json['products']
            .map<SingleOrderProduct>(
                (product) => SingleOrderProduct.fromJson(product))
            .toList();
}

class SingleOrderProduct {
  final int productId;
  final double price;
  final String productName;
  final String productImage;
  final int quantity;

  SingleOrderProduct(this.productId, this.productImage, this.price, this.productName,this.quantity);

  SingleOrderProduct.fromJson(Map<String, dynamic> json)
      : productId = json['id'],
        price = json['price'],
        productImage = json['imageUrl'],
        productName = json['productName'],
        quantity = json['quantity'];
}
