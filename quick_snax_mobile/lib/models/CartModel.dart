class CartItem {
  final int id;
  final String name;
  final String imageUrl;
  final double price;
  final int quantity;

 CartItem(this.id, this.name, this.imageUrl, this.price, this.quantity);

CartItem.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        name = json["name"],
        imageUrl = json['imageUrl'],
        price = json['price'],
        quantity = json['quantity'];

  Map<String, dynamic> toJson() => {'id': id,"name": name,'imageUrl' : imageUrl,
   'price': price,'quantity': quantity};
}

class CartList {
  final List<CartItem> cartItems;

  CartList(this.cartItems);

  CartList.fromJson(Map<String,dynamic> json)
  : cartItems = json['cartItems'].map<CartItem>((item) => 
  CartItem.fromJson(item)).toList();
  
   Map<String,dynamic> toJson() => {"cartItems":cartItems};
}
