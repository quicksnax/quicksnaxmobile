class Product {
  final int id;
  final String name;
  final String type;
  final double price;
  final int inStock;
  final String imageUrl;

  Product(
      this.id, this.name, this.type, this.price, this.inStock, this.imageUrl);

  Product.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        type = json['type'],
        price = json['price'],
        inStock = json['inStock'],
        imageUrl = json['imageUrl'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'type': type,
        'price': price,
        'inStock': inStock,
        'imageUrl': imageUrl
      };
}
