import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:quick_snax_mobile/models/models.dart';
import 'package:quick_snax_mobile/ui/custom_widgets/custom_widgets.dart';
import 'package:quick_snax_mobile/utils/constants.dart' as constants;
import 'package:quick_snax_mobile/utils/globals.dart' as globals;
import 'package:quick_snax_mobile/services/save_preferences.dart';

class OrdersController {
  static var token = globals.loggedInUser.token;

  static Future<OrdersByDate> getAllOrders() async {
    var token = globals.loggedInUser.token;
    try {
      var response = await http.get("${constants.Constants.ApiUrl}/order/list",
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $token"
          }).timeout(Duration(seconds: 20));
      print(response.statusCode);
      switch (response.statusCode) {
        case 200:
          {
            final responseBody =
                json.decode(response.body).cast<Map<String, dynamic>>();
            OrdersByDate orders = new OrdersByDate();
            orders.orders = responseBody
                .map<Orders>((json) => Orders.fromJson(json))
                .toList();
            return orders;
          }
          break;
        default:
          {
            throw Exception("Failed to load orders");
          }
      }
    } catch (e) {}
    return null;
  }

  static Future<SingleStudentOrderDetails> getStudentOrder(int id) async {
    var token = globals.loggedInUser.token;
    try {
      var response = await http.get("${constants.Constants.ApiUrl}/order/$id",
          headers: {
            HttpHeaders.authorizationHeader: "Bearer $token"
          }).timeout(Duration(seconds: 20));

      print(response.statusCode);
      switch (response.statusCode) {
        case 200:
          {
            final responseBody = json.decode(response.body);
            return SingleStudentOrderDetails.fromJson(responseBody);
          }
          break;
      }
    } catch (e) {
      return null;
    }
    return null;
  }

  static Future<bool> placeOrder(BuildContext context, String location) async {
    var token = globals.loggedInUser.token;
    try {
    var products = globals.list.cartItems.map((item) => NewOrderProducts(item.id,item.quantity)).toList();

    var order = new NewOrderModel(location);
    
    order.products = products;
    
    Map<String,dynamic> request = order.toJson();
    Map<String, String> headers = {'Content-Type': 'application/json',HttpHeaders.authorizationHeader: "Bearer $token"};
    
      final response = await http.post("${constants.Constants.ApiUrl}/order",
      headers: headers,
      body: jsonEncode(request))
      .timeout(Duration(seconds: 20));

print(response.statusCode);

      switch(response.statusCode){
        case 200:{
          globals.list.cartItems.clear();
          Preferences.removeFromCart(globals.list);
          return true;
        }
        break;
      case 403:
        {
          showAlertDialog(
              context, "Forbidden", "Forbidden access to resource", "Ok");
              return false;
        }
        break;
      case 415:
        {
          showAlertDialog(context, "Invalid request", "", "Ok");
          return false;
        }
        break;
      case 500:
        {
          showAlertDialog(context, "Something drastic happened", "", "Ok");
          return false;
        }
        break;
      default:
        {
          showAlertDialog(context, "OOPS!", "Something drastic happened", "Ok");
          return false;
          
        }
        break;
      }
      
    } catch (e) {
      print(e);
      showAlertDialog(context, e.toString(), e.toString(), "Ok");
      return false;
    }
  }

  static Future<bool> changeOrderStatus(BuildContext context,int orderId,String status) async{
    var requestBody = {
      "status": status,
    };
    var token = globals.loggedInUser.token;
    Map<String, String> headers = {'Content-Type': 'application/json',HttpHeaders.authorizationHeader: "Bearer $token"};
    try {
      final response = await http.post("${constants.Constants.ApiUrl}/order/update/$orderId",
          headers: headers,
          body: json.encode(status))
          .timeout(Duration(seconds: 20));

      print(response.statusCode);

      if (response.statusCode == 200) {
        return true;
      }

      showAlertDialog(context, "Oops!", "Status could not be updated", "Ok");
      return false;
    }catch(e){      
      print(e);
      return false;
    }

  }

  static Future<PreviousOrders> getMyOrders() async {
    if (globals.loggedInUser.role.compareTo(constants.Constants.Student) == 0) {
      var token = globals.loggedInUser.token;
      try {
        var response = await http
            .get("${constants.Constants.ApiUrl}/order/previous", headers: {
          HttpHeaders.authorizationHeader: "Bearer $token"
        }).timeout(Duration(seconds: 20));

        print(response.statusCode);

        switch (response.statusCode) {
          case 200:
            {
              final responseBody = json.decode(response.body);
              PreviousOrders orders = new PreviousOrders();
              orders.orders = responseBody
                  .map<PreviousOrdersModel>(
                      (json) => PreviousOrdersModel.fromJson(json))
                  .toList();
              return orders;
            }
            break;
          default:
            {
              throw Exception(response.body);
            }
        }
      } catch (e) {
        print(e);
      }
      throw Exception();
    }
    throw Exception();
  }
}
