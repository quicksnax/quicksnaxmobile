import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:quick_snax_mobile/models/models.dart';
import 'package:quick_snax_mobile/services/services.dart';
import 'package:quick_snax_mobile/ui/custom_widgets/custom_widgets.dart';
import 'package:quick_snax_mobile/ui/screens.dart';
import 'package:quick_snax_mobile/utils/constants.dart' as constants;
import 'package:quick_snax_mobile/utils/globals.dart' as globals;

Future<LoginModel> requestLogin(
    BuildContext context, String username, String password) async {
  Map<String, dynamic> requestBody = {
    "UserName": username,
    "Password": password
  };

  Map<String, String> headers = {'Content-Type': 'application/json'};
  try {
    final response = await http
        .post("${constants.Constants.ApiUrl}/auth/login",
            headers: headers, body: jsonEncode(requestBody))
        .timeout(Duration(seconds: 20));

    switch (response.statusCode) {
      case 200:
        {
          final responseBody = json.decode(response.body);
          Preferences.saveCurrentLogin(responseBody);
          var loginModel = LoginModel.fromJson(responseBody);
          var user = UserModel.createUser(loginModel.token);

          globals.loggedInUser = user;
          globals.list = new CartList(new List<CartItem>());
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => new HomeScreen()));
          return loginModel;
        }
        break;
      case 401:
        {
          showAlertDialog(
              context, "Unauthorized", "You are unauthorized to login", "Ok");
        }
        break;
      case 403:
        {
          showAlertDialog(
              context, "Forbidden", "Forbidden access to resource", "Ok");
        }
        break;
      case 415:
        {
          showAlertDialog(context, "Invalid media", "", "Ok");
        }
        break;
      case 500:
        {
          showAlertDialog(context, "Something drastic happened", "", "Ok");
        }
        break;
      default:
        {
          showAlertDialog(context, "Something happened", "", "Ok");
        }
        break;
    }
  } catch (e) {
    showAlertDialog(context, e.toString(), e.toString(), "Ok");
  }
  return null;
}
