import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:quick_snax_mobile/models/models.dart';
import 'package:quick_snax_mobile/ui/custom_widgets/custom_widgets.dart';
import 'package:quick_snax_mobile/utils/constants.dart' as constants;
import 'package:quick_snax_mobile/utils/globals.dart' as globals;
import 'package:quick_snax_mobile/viewmodels/SnaxViewModel.dart';

class ProductController {
  static var token = globals.loggedInUser.token;

  static Future<SnaxViewModel> getProducts() async {
    var token = globals.loggedInUser.token;
    try {
      var response = await http
          .get("${constants.Constants.ApiUrl}/products/list", headers: {
        HttpHeaders.authorizationHeader: "Bearer $token"
      }).timeout(Duration(seconds: 20));

      switch (response.statusCode) {
        case 200:
          {
            final responseBody =
                json.decode(response.body).cast<Map<String, dynamic>>();
            SnaxViewModel snaxViewModel = new SnaxViewModel();
            snaxViewModel.snaxList = responseBody
                .map<Product>((json) => Product.fromJson(json))
                .toList();
            return snaxViewModel;
          }
          break;

        case 403:
          {
            return null;
          }
      }
    } catch (e) {
      return null;
    }
    return null;
  }

  static Future addNewProduct(
      BuildContext context,
      String productName,
      double productPrice,
      String productType,
      int productQuantity,
      File productImage) async {
    Map<String, String> requestBody = {
      "Name": productName,
      "InStock": productQuantity.toString(),
      "Type": productType,
      "Price": productPrice.toString()
    };
    var token = globals.loggedInUser.token;
    var url = Uri.parse("${constants.Constants.ApiUrl}/products");
    var request = new http.MultipartRequest("POST", url);
    request.headers.addAll({HttpHeaders.authorizationHeader: "Bearer $token"});
    request.fields.addAll(requestBody);
    var file = await http.MultipartFile.fromPath(
        "ProductImage", productImage.path,
        filename: productImage.path,
        contentType: new MediaType("image", "jpg"));
    request.files.add(file);
    var response = await request.send();
    if (response.statusCode == 200) {
      Navigator.of(context).pop();
      return;
    }
    print(response.statusCode);
    response.stream.transform(utf8.decoder).listen((value) {
      print(value);
    });
    showAlertDialog(context, "Oops!", "Product could not be added", "Ok");
  }

  static Future editProduct(
      int id,
      BuildContext context,
      String productName,
      double productPrice,
      String productType,
      int productQuantity,
      File productImage) async {
    Map<String, String> requestBody = {
      "Name": productName,
      "InStock": productQuantity.toString(),
      "Type": productType,
      "Price": productPrice.toString()
    };
    var token = globals.loggedInUser.token;
    var url = Uri.parse("${constants.Constants.ApiUrl}/products/$id");
    var request = new http.MultipartRequest("PUT", url);
    request.headers.addAll({HttpHeaders.authorizationHeader: "Bearer $token"});
    request.fields.addAll(requestBody);
    var file = await http.MultipartFile.fromPath(
        "ProductImage", productImage.path,
        filename: productImage.path,
        contentType: new MediaType("image", "jpg"));
    request.files.add(file);
    var response = await request.send().timeout(Duration(seconds: 20));
    if (response.statusCode == 200) {
      Navigator.of(context).pop();
      return;
    }
    print(response.statusCode);
    response.stream.transform(utf8.decoder).listen((value) {
      print(value);
    });
    showAlertDialog(context, "Oops!", "Product could not be updated", "Ok");
  }

static Future<bool> deleteProduct(BuildContext context,int id)async{
   Map<String, String> headers = {'Content-Type': 'application/json',HttpHeaders.authorizationHeader: "Bearer $token"};
   try {
     var token = globals.loggedInUser.token;
      final response = await http.delete("${constants.Constants.ApiUrl}/products/$id",
          headers: headers,)
          .timeout(Duration(seconds: 20));

      print(response.statusCode);

      if (response.statusCode == 200) {
        return true;
      }

      showAlertDialog(context, "Oops!", "Product could not be deleted", "Ok");
      return false;
    }catch(e){      
      print(e);
      return false;
    }
}

}
