import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:quick_snax_mobile/models/RegistrationModel.dart';
import 'package:quick_snax_mobile/ui/screens.dart';
import 'package:quick_snax_mobile/utils/constants.dart' as constants;

Future registerAsync(BuildContext context, RegistrationModel model) async {
  Map<String, String> headers = {'Content-Type': 'application/json'};
  if (model.password.compareTo(model.confirmPassword) != 0) {
    showAlertDialog(context, "Passwords do not match", "", "Okay");
    return;
  }
  Map<String, dynamic> requestBody = model.toJson();
  try {
    final response = await http
        .post("${constants.Constants.ApiUrl}/account/register",
            headers: headers, body: jsonEncode(requestBody))
        .timeout(Duration(seconds: 60));

    switch (response.statusCode) {
      case 200:
        {
          Navigator.of(context).pushReplacementNamed('/LoginScreen');
          break;
        }
      case 401:
        {
          showAlertDialog(
              context, "Unauthorized", "You are unauthorized to login", "Ok");
        }
        break;
      case 403:
        {
          showAlertDialog(
              context, "Forbidden", "Forbidden access to resource", "Ok");
        }
        break;
      case 415:
        {
          showAlertDialog(context, "Invalid media", "", "Ok");
        }
        break;
      case 500:
        {
          showAlertDialog(context, "Something drastic happened", "", "Ok");
        }
        break;
      default:
        {
          showAlertDialog(context, "OOPS!", "Something drastic happened", "Ok");
        }
        break;
    }
  } catch (e) {
    showAlertDialog(context, e.toString(), e.toString(), "Ok");
  }
}
